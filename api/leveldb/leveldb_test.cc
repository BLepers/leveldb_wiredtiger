/*-
 * Public Domain 2008-2014 WiredTiger, Inc.
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <assert.h>
#include <iostream>
#include <unistd.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "leveldb_wt.h"
#include "random.h"

#include <omp.h>

#ifdef __x86_64__
#define rdtscll(val) {                                           \
       unsigned int __a,__d;                                        \
       asm volatile("rdtsc" : "=a" (__a), "=d" (__d));              \
       (val) = ((unsigned long)__a) | (((unsigned long)__d)<<32);   \
}
#else
#define rdtscll(val) __asm__ __volatile__("rdtsc" : "=A" (val))
#endif

#define declare_timer uint64_t elapsed; \
   struct timeval st, et;

#define start_timer gettimeofday(&st,NULL);

#define stop_timer(msg, args...) ;do { \
   gettimeofday(&et,NULL); \
   elapsed = ((et.tv_sec - st.tv_sec) * 1000000) + (et.tv_usec - st.tv_usec) + 1; \
   printf("(%s,%d) [%6lums] " msg "\n", __FUNCTION__ , __LINE__, elapsed/1000, ##args); \
} while(0)

using namespace std;

leveldb::DB* db;
int DB_ITEM_COUNT = 100000000;
//int DB_ITEM_COUNT = 1000000;
pthread_barrier_t barrier;

extern "C" void * do_yccsb(void* dummy) {
  long tid = (long)dummy;
  leveldb::Status s;

  init_seed();

  std::string value_read;
  char key[100];
  char val[1024];
  snprintf(val, sizeof(val), "%1000d", 0);

  long int OPS_COUNT_PER_THREAD = 1000000;
  int k;
  declare_timer;

  for (int j = 0; j < 2; j++){ // 0 = uniform, 1 = zipf

    //YCSB A and F
    pthread_barrier_wait(&barrier);
    cout << " YCSB A and F " << j << "\n";
    start_timer {
      uint64_t real_start, start, last, nb_requests = 0;
      rdtscll(real_start);
      rdtscll(start);

      for (int i = 0; i < OPS_COUNT_PER_THREAD; i++) {

        if (j == 0) //uniform
          k = uniform_next();
        else //zipf
          k = zipf_next();
        snprintf(key, sizeof(key), "%016d", k);
        //if(i < 10)
        //   printf("%ld %d %d\n", tid, i, k);

        long op = uniform_next() % 2;
        if (op == 0){// Write
          s = db->Put(leveldb::WriteOptions(), key, val);
        } else {//Read
          s = db->Get(leveldb::ReadOptions(), key, &value_read);
        }


        /*if(i%10 == 0) {
             OperationContext *context = db->GetContextHack();
             WT_SESSION *session = context->GetSession();
             session->log_flush();
        }*/

        rdtscll(last);
        nb_requests++;
        if(cycles_to_us(last - start) > 1000000) {
           printf("INJECTOR %lu req/s (Time from start %lus)\n", nb_requests*1000000/cycles_to_us(last - start), cycles_to_us(last - real_start)/1000000LU);
           nb_requests = 0;
           start = last;
        }
      }
      assert(s.ok());
    }stop_timer("YCSB A [%s] - Time for %lu accesses = %lums (%lu ops/s)", j?"Zipf":"Uniform", OPS_COUNT_PER_THREAD, elapsed/1000, OPS_COUNT_PER_THREAD*1000000LU/elapsed);

    //YCSB B and D
    pthread_barrier_wait(&barrier);
    cout << " YCSB B and D " << j << "\n";
    start_timer {
      uint64_t real_start, start, last, nb_requests = 0;
      rdtscll(real_start);
      rdtscll(start);

      for (int i = 0; i < OPS_COUNT_PER_THREAD; i++){

        if (j == 0) //uniform
          k = uniform_next();
        else //zipf
          k = zipf_next();
        snprintf(key, sizeof(key), "%016d", k);
        //snprintf(val, sizeof(val), "%1000d", k);

        long op = uniform_next() % 100;
        if (op < 5){// Write
          s = db->Put(leveldb::WriteOptions(), key, val);
        } else {//Read
          s = db->Get(leveldb::ReadOptions(), key, &value_read);
        }

        rdtscll(last);
        nb_requests++;
        if(cycles_to_us(last - start) > 1000000) {
           printf("INJECTOR %lu req/s (Time from start %lus)\n", nb_requests*1000000/cycles_to_us(last - start), cycles_to_us(last - real_start)/1000000LU);
           nb_requests = 0;
           start = last;
        }
      }
      assert(s.ok());
    }stop_timer("YCSB B D [%s] - Time for %lu accesses = %lums (%lu ops/s)", j?"Zipf":"Uniform", OPS_COUNT_PER_THREAD, elapsed/1000, OPS_COUNT_PER_THREAD*1000000LU/elapsed);

    //YCSB C
    pthread_barrier_wait(&barrier);
    cout << " YCSB C " << j << "\n";
    start_timer {
      uint64_t real_start, start, last, nb_requests = 0;
      rdtscll(real_start);
      rdtscll(start);

      for (int i = 0; i < OPS_COUNT_PER_THREAD; i++){

        if (j == 0) //uniform
          k = uniform_next();
        else //zipf
          k = zipf_next();
        snprintf(key, sizeof(key), "%016d", k);
        s = db->Get(leveldb::ReadOptions(), key, &value_read);

        rdtscll(last);
        nb_requests++;
        if(cycles_to_us(last - start) > 1000000) {
           printf("INJECTOR %lu req/s (Time from start %lus)\n", nb_requests*1000000/cycles_to_us(last - start), cycles_to_us(last - real_start)/1000000LU);
           nb_requests = 0;
           start = last;
        }
      }
      assert(s.ok());
      usleep(1000000);
    }stop_timer("YCSB C [%s] - Time for %lu accesses = %lums (%lu ops/s)", j?"Zipf":"Uniform", OPS_COUNT_PER_THREAD, elapsed/1000, OPS_COUNT_PER_THREAD*1000000LU/elapsed);


    //YCSB E
    pthread_barrier_wait(&barrier);
    cout << " YCSB E " << j << "\n";
    start_timer {
      uint64_t real_start, start, last, nb_requests = 0;
      rdtscll(real_start);
      rdtscll(start);

#define SCAN_REDUCE_RATIO 70
      for (int i = 0; i < OPS_COUNT_PER_THREAD/SCAN_REDUCE_RATIO; i++){

        if (j == 0) //uniform
          k = uniform_next();
        else //zipf
          k = zipf_next();
        snprintf(key, sizeof(key), "%016d", k);

        long op = uniform_next() % 100;
        if (op < 5){// Write
          s = db->Put(leveldb::WriteOptions(), key, val);
        } else {//Scan
          long scan_size = uniform_next() % 100;
          //cout << "Scan size: " << scan_size << "\n";

          leveldb::ReadOptions read_options;
          read_options.snapshot = db->GetSnapshot();
          leveldb::Iterator* iter = db->NewIterator(read_options);
          int retrieved = 0;
          for (iter->Seek(key); iter->Valid() && retrieved < scan_size; iter->Next()) {
            retrieved ++;
            //cout << iter->key().ToString() << ": ";
          }
          delete iter;
          db->ReleaseSnapshot(read_options.snapshot);
          //cout << "\n";

        }
        rdtscll(last);
        nb_requests++;
        if(cycles_to_us(last - start) > 1000000) {
           printf("INJECTOR %lu req/s (Time from start %lus)\n", nb_requests*1000000/cycles_to_us(last - start), cycles_to_us(last - real_start)/1000000LU);
           nb_requests = 0;
           start = last;
        }
      }
      assert(s.ok());

    } stop_timer("YCSB E [%s] - Time for %lu accesses = %lums (%lu ops/s)", j?"Zipf":"Uniform", OPS_COUNT_PER_THREAD, elapsed/1000, OPS_COUNT_PER_THREAD/SCAN_REDUCE_RATIO*1000000LU/elapsed);


  }



  // Read through the main database
  // cout << "Read main database:" << endl;
  // leveldb::ReadOptions read_options;
  // read_options.snapshot = db->GetSnapshot();
  // leveldb::Iterator* iter = db->NewIterator(read_options);
  // for (iter->SeekToFirst(); iter->Valid(); iter->Next()) {
  //   cout << iter->key().ToString() << ": "  << iter->value().ToString() << endl;
  // }

  // delete iter;
  // db->ReleaseSnapshot(read_options.snapshot);

  //delete db;
  //
  return NULL;
}

extern "C" int main() {
  int NB_THREADS = 16;
  leveldb::Options options;
  options.create_if_missing = true;

  declare_timer;
  start_timer;
  leveldb::Status s = leveldb::DB::Open(options, "/scratch0/wiredtiger", &db);
  stop_timer("Time to open the DB\n");

  printf("Init zipf\n");
  init_zipf_generator(0, DB_ITEM_COUNT);
  printf("Init zipf done\n");

  //Initialization
  /*char key[100];
  char val[1024];
  for (int i = 0; i < DB_ITEM_COUNT; i++){
     snprintf(key, sizeof(key), "%016d", i);
     snprintf(val, sizeof(val), "%1000d", i);
     s = db->Put(leveldb::WriteOptions(), key, val);

  //   //cout << "Rand: " << uniform_next() << " Zipf: " << zipf_next() << "\n";
  }
  assert(s.ok());*/


  pthread_barrier_init(&barrier,NULL,NB_THREADS);
  pthread_t threads[NB_THREADS];
  for(size_t i = 0; i < NB_THREADS; i++) {
    //struct pdata *data = malloc(sizeof(*data));
    //data->...
    pthread_create(&threads[i], NULL, do_yccsb, (void*)(long)i);
  }

  system("iostat -p 1 5500 > log_io &");
  for(size_t i = 0; i < NB_THREADS; i++) {
    pthread_join(threads[i], NULL);
  }
  system("killall iostat");
}

