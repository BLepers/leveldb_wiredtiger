#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

my $last_bench;
my $last_time = 0;
my $last_total = 0;
my %grand_total;
while(<>) {
	#INJECTOR 2425 req/s (Time from start 8s)
	#(do_yccsb,237) [ 10815ms] YCSB E [Zipf] - Time for 1000000 accesses = 10815ms (1320 ops/s)
	if(/YCSB (\w).*(\d)$/) {
		print "YCSB $1 - ".($2?"Zipf":"Uniform")."\n";
	} elsif(/INJECTOR (\d+) req.*start (\d+)s/) {
		if($2 != $last_time) {
			print "$last_time\t$last_total\n" if($last_time != 0);
			for(my $t = $last_time+1; $t < $2; $t++) {
				print "$t\t0\n";
			}
			$last_time = $2;
			$last_total = 0;
		}
      #print "\t$1\t$2\n";
		$last_total += $1;
	} elsif(/YCSB (\w).*?(Zipf|Uniform).*?(\d+) ops/) {
		$grand_total{"$1-$2"} += $3;
	}
}
print Dumper(\%grand_total);
