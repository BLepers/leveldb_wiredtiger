#ifndef RANDOM_H
#define RANDOM_H 1

typedef long (*random_gen_t)(void);

unsigned long xorshf96(void);

void init_seed(void);
void init_zipf_generator(long min, long max);
long zipf_next(); // zipf distribution, call init_zipf_generator first
long uniform_next(); // uniform, call init_zipf_generator first
long bogus_rand(); // returns something between 1 and 1000

const char *get_function_name(random_gen_t f);

uint64_t cycles_to_us(uint64_t cycles);

#endif
